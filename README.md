
# Vim configs of kalnar

## About

I started to create separate projects for all my configs to be able to clone only one if needed. This way in any guest session I can clone my vim configs and plugins and have a cool vim config in 1-2 minutes.

## Installation

you need to run:

```
initialize-vim.sh
```

then:

```
create.sym.link.for.vim.sh
```

then you need to install plugins by `:PluginInstall`

