#!/bin/bash

#if [[ "${BACKED_UP_HOME+x}" != "x" ]];then 
  #BACKED_UP_HOME=$HOME/; 
#fi

if ! command -v greadlink &> /dev/null; then 
  READ_LINK_COMMAND=readlink
else 
  READ_LINK_COMMAND=greadlink
fi

VIMCONFIG=$( dirname `$READ_LINK_COMMAND -f "${BASH_SOURCE[0]}"` )

if [[ -f ~/.vimrc ]];then
  mv ~/.vimrc vimrc_before_update
fi

if [[ -f ~/.vim ]];then
  mv ~/.vim vim_before_update
fi

ln -sf ${VIMCONFIG}/vim.home ~/.vim
ln -sf ${VIMCONFIG}/vimrc ~/.vimrc
