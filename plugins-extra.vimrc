"""""""""""""""""""
"""""""""""""""""""
""PLUGINS"SECTION""
"""""""""""""""""""
"""""""""""""""""""
" plugins vundle - plugin manager:
"
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" plugins:
Plugin 'VundleVim/Vundle.vim'
Plugin 'Chiel92/vim-autoformat'
"Plugin 'wincent/command-t'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'mattn/emmet-vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'majutsushi/tagbar'
"Plugin 'xolox/vim-easytags'
Plugin 'lvht/tagbar-markdown'
Plugin 'xolox/vim-misc'
"Plugin 'vim-latex/vim-latex'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'xolox/vim-notes'
Plugin 'easymotion/vim-easymotion'
Plugin 'dhruvasagar/vim-table-mode'
"Plugin 'powerline/powerline'
" I couldn't get it to work, instead will try vim-airline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" autoadjust tab setting based on file, not sure that I need this
"Plugin 'tpope/vim-sleuth'
Plugin 'tpope/vim-fugitive'
Plugin 'kien/ctrlp.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'pangloss/vim-javascript'
Plugin 'junegunn/goyo.vim'
Plugin 'vim-scripts/LanguageTool'
Plugin 'flazz/vim-colorschemes'
Plugin 'jacoborus/tender.vim'
" markdiwn live preview
Plugin 'shime/vim-livedown'
"Plugin 'tex/vimpreviewpandoc'
Plugin 'dylon/vim-antlr'
"Plugin 'malithsen/trello-vim'
Plugin 'adimit/prolog.vim'
Plugin 'vim-pandoc/vim-pandoc'
Plugin 'vim-pandoc/vim-pandoc-syntax'
Plugin 'itchyny/calendar.vim'
" python plugin YouCompleteMe
"Plugin 'Valloric/YouCompleteMe'
" YouCompleteMe requiries vim 7.4.15+, ubuntu14 ships with 7.4
"Plugin 'iamcco/mathjax-support-for-mkdp'
"Plugin 'iamcco/markdown-preview.vim'
Plugin 'epeli/slimux'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'tmux-plugins/vim-tmux-focus-events'
"
" fuzzy finder
set rtp+=~/.fzf

call vundle#end()            " required

imap <C-v> <Plug>IMAP_JumpForward

" slimux mappings
map <Leader>r :SlimuxREPLSendLine<CR>
vmap <Leader>r :SlimuxREPLSendSelection<CR>
nmap <Leader>sr :SlimuxREPLConfigure<CR>

" vim-tmux-navigator mapings
let g:tmux_navigator_no_mappings = 1

nmap <C-S-h> :TmuxNavigateLeft<cr>
nmap <C-S-j> :TmuxNavigateDown<cr>
nmap <C-S-k> :TmuxNavigateUp<cr>
nmap <C-S-l> :TmuxNavigateRight<cr>
nmap <C-S-.> :TmuxNavigateDown<cr>

