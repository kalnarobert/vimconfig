#!/bin/bash

#if [[ "${BACKED_UP_HOME+x}" != "x" ]];then 
  #BACKED_UP_HOME=$HOME/; 
#fi

# my default vim config directory
#VIMCONFIG=${BACKED_UP_HOME}config/vim

# to use the project directory as VIMCONFIG variable:
if ! command -v greadlink &> /dev/null; then 
  READ_LINK_COMMAND=readlink
else 
  READ_LINK_COMMAND=greadlink
fi

VIMCONFIG=$( dirname `$READ_LINK_COMMAND -f "${BASH_SOURCE[0]}"` )

mkdir -p ${VIMCONFIG}/vim.home

git clone https://github.com/VundleVim/Vundle.vim.git ${VIMCONFIG}/vim.home/bundle/Vundle.vim

${VIMCONFIG}/create.sym.link.for.vim.sh

vim +PluginInstall +qall

git clone https://github.com/jszakmeister/markdown2ctags.git ${VIMCONFIG}/markdown2ctags/
