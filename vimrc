
source ${BACKED_UP_HOME}config/vim/vimrc-minimal

source ${BACKED_UP_HOME}config/vim/plugins-extra.vimrc

autocmd Filetype markdown,md,pandoc setlocal textwidth=80 formatoptions=tcq

let g:calendar_google_calendar = 1
let g:calendar_google_task = 1

filetype plugin indent on    " required

" enable filetype plugin for nerdcommenter
filetype plugin on

let g:tagbar_type_tex = {
    \ 'ctagstype' : 'latex',
    \ 'kinds'     : [
        \ 's:sections',
        \ 'g:graphics:0:0',
        \ 'l:labels',
        \ 'r:refs:1:0',
        \ 'p:pagerefs:1:0'
    \ ],
    \ 'sort'    : 0,
\ }
