"""""""""""""""""""
"""""""""""""""""""
""PLUGINS"SECTION""
"""""""""""""""""""
"""""""""""""""""""
" plugins vundle - plugin manager:
"
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" plugins:
"Plugin 'VundleVim/Vundle.vim'
"Plugin 'Chiel92/vim-autoformat'
"Plugin 'wincent/command-t'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
"Plugin 'mattn/emmet-vim'
"Plugin 'vim-syntastic/syntastic'
Plugin 'majutsushi/tagbar'
"Plugin 'lvht/tagbar-markdown'
"Plugin 'xolox/vim-easytags'
"Plugin 'xolox/vim-misc'
"Plugin 'vim-latex/vim-latex'
"Plugin 'nathanaelkane/vim-indent-guides'
"Plugin 'xolox/vim-notes'
Plugin 'dhruvasagar/vim-table-mode'
"Plugin 'powerline/powerline'
" I couldn't get it to work, instead will try vim-airline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" autoadjust tab setting based on file, not sure that I need this
"Plugin 'tpope/vim-sleuth'
"Plugin 'tpope/vim-fugitive'
Plugin 'kien/ctrlp.vim'
"Plugin 'altercation/vim-colors-solarized'
"Plugin 'pangloss/vim-javascript'
"Plugin 'junegunn/goyo.vim'
"Plugin 'vim-scripts/LanguageTool'
Plugin 'flazz/vim-colorschemes'
Plugin 'jistr/vim-nerdtree-tabs'
"Plugin 'jacoborus/tender.vim'
" markdiwn live preview
"Plugin 'shime/vim-livedown'
"Plugin 'tex/vimpreviewpandoc'
"Plugin 'dylon/vim-antlr'
"Plugin 'malithsen/trello-vim'
"Plugin 'adimit/prolog.vim'
"Plugin 'valloric/YouCompleteMe'
Plugin 'vim-pandoc/vim-pandoc'
Plugin 'vim-pandoc/vim-pandoc-syntax'
"Plugin 'iamcco/mathjax-support-for-mkdp'
"Plugin 'iamcco/markdown-preview.vim'
" fuzzy finder
"set rtp+=~/.fzf

call vundle#end()            " required

let g:airline#extensions#tabline#formatter = 'unique_tail'

